(ns thynktube.server
  (:gen-class :main true)
  (:use
   [ring.util.response :as resp]
   [ring.middleware.resource :only [wrap-resource]]
   [ring.middleware.file-info :only [wrap-file-info]]
   org.httpkit.server
   [ring.middleware.session :only [wrap-session]]
   [ring.middleware.json :only [wrap-json-response wrap-json-body]]
   [ring.util.response :only [response file-response]]
   [clojure.core.match :only (match)]
   compojure.core
   compojure.handler
   )
  (:require
   monger.joda-time
   [ring.middleware.reload :as reload]
   [clojure.core.async :refer [<! >! put! close! go chan mult tap untap]]
   [monger.ring.session-store :refer [session-store]]
   [monger.core :as mg]
   [monger.collection :as mc]
   [monger.query :refer :all]
   [clojure.data.json :as json]
   [org.httpkit.client :as http]
   [compojure.route :refer [resources]]
   [clj-time.core :as t]
   [clj-time.format :as tf]
   [clj-time.coerce :as coe]
   [clojure.string :as s]
   [thynktube.utils :refer [generate-nick parse-youtube-url]]
   [thynktube.storage :refer [load-chatlog load-playlist]]
   )

  (:refer-clojure :exclude [sort find])
  (:import [com.mongodb MongoOptions ServerAddress]
           [java.util TimerTask Timer]))

(declare playlist)
(declare timer-task)
(def current-task (atom nil))
(def task-timer (atom nil))
(def clients (atom []))
(def arbiter (atom #{}))

(def commands (chan))
(def cmd-mult (mult commands))

(def chat-commands (chan))
(def chat-commands-mult (mult chat-commands))


(defn stringify-timestamp [msg]
  (assoc msg
    :added
    (tf/unparse
     (tf/formatters :hour-minute-second)
     (:added msg))))



(add-watch clients :channels-watcher
           (fn [k r old-state new-state]
             (do
               (when (= (count old-state) 0)
                 (println "Thynktube started"))
               (when (= (count new-state) 0)
                 (println "No clients remain.")
                 )
               (go (>! chat-commands [:chat-users (vec (map #(:nick %) new-state))]))
               (println k new-state))))


(defn timer-task []
  (if (> (count @playlist) 0)
    (let [current-video (first @playlist)]
      (reset! arbiter #{})
      (go (>! commands [:next-video (:id current-video)
                        :duration (:duration current-video)]))

      (let [task (proxy [TimerTask] []
                   (run
                    [] (do
                         (println "Next video")
                         (mc/remove "playlist" {:order (:order current-video)})
                         (reset! playlist (load-playlist))
                         (timer-task))))]
        (reset! current-task task)


        (reset! task-timer (new Timer))
        (. @task-timer
           (schedule task
                     (coe/to-date
                      (t/plus (t/now)
                              (t/seconds
                               (:duration current-video))))))))
    (do
      (go (>! commands [:reload-playlist]))
      (reset! current-task nil))))

(defn run-next-video []
  ;;Stop current timer
  (println "Vote finished, run next video")
  (when @task-timer
    (doto @task-timer
      (.cancel)
      (.purge)))

  (when (> (count @playlist) 0)
    (let [current-video (first (with-collection
                                 "playlist"
                                 (sort (sorted-map :order 1))
                                 (limit 1)))]
      (println current-video)
      (mc/remove-by-id "playlist" (:_id current-video))
      (reset! playlist (load-playlist))
      (future (timer-task))
      )))


(mg/connect!)
(mg/set-db! (mg/get-db "thynktube"))





(def chatlog (atom (load-chatlog)))
(def playlist (atom (load-playlist)))

(add-watch playlist :update-watcher
           (fn [k r old-state new-state]
             (go (>! commands [:reload-playlist])
                 (if (and (= (count old-state) 0)
                          (> (count new-state) 0))
                   (future (timer-task))))))


(defn main-page [request]
  (let [session (:session request)
        nick (or (:nick session) (generate-nick))
        session (assoc session :nick nick)]
    (-> (resource-response "public/index.html")
        (assoc :session session))))


(defn user-leave [userlist nick]
  (let [user (first (filter #(= nick (:nick %)) userlist))
        user-count (:count user)]
    (vec (if (> user-count 1)
           (map (fn [x] (if (= (:nick x) nick)
                          {:nick nick :count (- user-count 1)}
                          x)) userlist)
           (filter #(not= (:nick %) nick)  userlist)))))

(defn user-join [userlist nick]
  (let [user (first (filter #(= nick (:nick %)) userlist))
        user-count (:count user)]
    (vec (if (and user user-count)
           (map (fn [x] (if (= (:nick x) nick)
                          {:nick nick :count (+ user-count 1)}
                          x
                          )) userlist)
           (conj userlist {:nick nick :count 1})))))


(defn ws-handler [request]
  (with-channel request channel
    (println "Control channel opened")

    (let [session (:session request)
          nick (:nick session)
          queue (chan)]
      ;;Добавляем нового клиента
      (swap! clients user-join nick)

      ;;Мультиплексируем оповещения
      (tap cmd-mult queue)

      (send! channel (str [:connected "ok" :nick nick]))

      (when (not (nil? @current-task))
        ;;Если уже идет проигрывание - высчитываем текущее положение и отправляем его клиенту
        (let [video (first @playlist)
              video-id (:id video)
              duration (:duration video)
              next-time (.scheduledExecutionTime @current-task)
              now (System/currentTimeMillis)
              offset (- duration
                        (quot (- next-time now) 1000))]
          (send! channel (str [:start-video video-id
                               :duration duration
                               :offset offset]))))


      (go
       (loop []
         (let [command (<! queue)]
           (when-not (= command nil)
             (println "Control message sent" (str command))
             (send! channel (str command))
             (recur)))))




      (on-close channel (fn [status]
                          (println "channel closed: " status)
                          (untap cmd-mult queue)
                          (close! queue)
                          (swap! clients user-leave nick)))
      (on-receive channel (fn [data] ;; echo it back
                            (send! channel (str [:ping "pong"])))))))



(defn playlist-handler [request]
  (with-channel request channel
    (let [session (:session request)]
      (send! channel
             {:status 200
              :headers {"Content-Type" "application/edn"}
              :body (pr-str @playlist)}))))










(defn save-video [title duration video-id]
  (let [last-order (if (not= (count @playlist) 0)
                     (+ 1 (-> @playlist last :order)) 1)
        now (tf/unparse (tf/formatters :hour-minute-second) (t/now))
        ]
    ;;Save new video to mongo
    (mc/insert "playlist" {:id video-id
                           :title title
                           :duration duration
                           :added now
                           :order last-order})
    ;;and reload playlist
    (reset! playlist (load-playlist))
    [:id video-id :title title :duration duration]))


(defn add-video [video-id]
  (let [{:keys [status headers body error]}
        @(http/get (format "https://gdata.youtube.com/feeds/api/videos/%s?v=2&alt=jsonc" video-id))
        resp (json/read-str body :key-fn keyword)
        error (:error resp)
        data (:data resp)
        allowed (= (-> data :accessControl :embed) "allowed")
        title (:title data)
        duration (:duration data)
        ]
    (if error
      [:error "Видео не существует"]
      (if (> duration (* 30 60))
        [:error "Видео слишком длинное"]
        (if (not allowed)
          [:error "Видео нельзя встраивать"]
          (save-video title duration video-id)
          )))))


(defn add-playlist [playlist-id]
  (let [{:keys [status headers body error]}
        @(http/get (format "https://gdata.youtube.com/feeds/api/playlists/%s?alt=jsonc&v=2" playlist-id))
        resp (json/read-str body :key-fn keyword)
        error (:error resp)
        data (:data resp)
        items (:items data)
        videos (vec (map #(:video %) items))
        allowed-videos (filter #(= (-> % :accessControl :embed) "allowed") videos)
        args (vec (map #(do [(:title %)
                          (:duration %)
                          (:id %)]) allowed-videos))
        ]
    (if error
      [:error "Плейлист не существует"]
      (do
        (doall (map #(apply save-video %) args))
        [:result "ok"]
        ))))











(defn new-video-handler [request video-url]
  "Добавление нового видео"
  (if-not  (-> request :session :nick)
    ;;Check login
    (let [error-msg [:error  "Вы не залогинены, перезагрузите страницу"]]
      (response (str error-msg))
      (go (>! commands error-msg)))
    (let [url-data (parse-youtube-url video-url)
          msg (match url-data
                     [:video video-id] (add-video video-id) ;;TODO get data from api and add video
                     [:playlist playlist-id] (add-playlist playlist-id) ;;TODO get data from api and add videos from playlist
                     [:unknown] [:error "Неправильная ссылка"] ;;TODO return error
                     )
          ]
      (go (>! commands msg))
      (response (str msg)))))





(defn chat-ws-handler [request]
  (with-channel request channel
    (println "Channel opened")

    (let [session (:session request)
          nick (:nick session)
          chat-queue (chan)]

      ;;Мультиплексируем оповещения
      (tap chat-commands-mult chat-queue)

      (send! channel (str [:users (vec (map #(:nick %) @clients))
                           :messages @chatlog]))
      (go
       (loop []
         (let [command (<! chat-queue)]
           (when-not (= command nil)
             (println "Chat message sent" (str command))
             (send! channel (str command))
             (recur)))))

      (on-close channel (fn [status]
                          (println "channel closed: " status)
                          (untap chat-commands-mult chat-queue)
                          (close! chat-queue)))
      (on-receive channel (fn [data] ;; echo it back
                            (send! channel (str [:ping "pong"])))))))



(defn show-chat-message [msg]
  (mc/insert "chatlog" msg)

  ;;Очистка старыХ логов
  (when (> (mc/count "chatlog") 20)
    (doall
     (map #(mc/remove-by-id "chatlog" (:_id %))
          (with-collection
            "chatlog"
            (sort (sorted-map :added -1))
            (skip 20)))))
  (reset! chatlog (load-chatlog))
  (go (>! chat-commands (str [:new-message
                              (stringify-timestamp msg)]))))


(defn vote-for-skip [nick]
  (when (and (not (contains? @arbiter nick))
             (> (count @playlist) 1))
    (swap! arbiter conj nick)
    (let [voted-count (count @arbiter)
          all-count (count @clients)]
      (println "Contra" voted-count "all" all-count)
      (go (>! chat-commands [:new-message {:nick "System"
                                           :message
                                           (str nick " проголосовал за пропуск видео, всего "
                                                voted-count " голосов из " all-count)}]))
      (when
        (or (= all-count 0)
            (> (/ (float voted-count) all-count) 0.5))
        (run-next-video)
        (reset! arbiter #{})))))


(defn process-chat-message [new-message]
  (let [nick (:nick new-message)
        message (:message new-message)
        msg-chunks (s/split message #"\s+")]
    (match msg-chunks
           ["/help" & _]  (go (>! chat-commands
                                  [:new-message
                                   {:message "/help - show this help; /next - vote for skip"
                                    :nick "System"}]))
           ["/next" & _] (vote-for-skip nick) ;;begin voting, if votes >50% -> stop timer,
           ;;remove first video from playlist, start timer
           :else (show-chat-message new-message))))





(defn newmessage-handler [request]
  (let [{{nick :nick} :session
         {message :message} :params} request]
    (if-not nick
      ;; Если не залогинен

      (let [error-msg [:error "Вы не залогинены, перезагрузите страницу"]]
        (response (str error-msg))
        (go (>! commands error-msg)))

      (let [new-message {:nick nick :message message :added (t/now)}]
        (process-chat-message new-message)
        (response (str [:result "ok"]))))))

(defroutes handler
  (resources "/static")
  (GET "/ws" [] ws-handler)
  (GET "/ws/chat" [] chat-ws-handler)
  (GET "/playlist" [] playlist-handler)
  (POST "/addvideo" [video-url :as request] (new-video-handler request video-url))
  (POST "/addmessage" [] newmessage-handler)
  (GET "/" [] main-page))



(def app (-> handler
             wrap-json-body
             (wrap-json-response {:keywords? true :bigdecimals? true})
             wrap-file-info
             ;;(wrap-resource "public")
             (wrap-session {:store (session-store "sessions")})
             site))

(defn in-dev? [& args] true)


(defn -main [& args]
  (let [handler (if (in-dev? args)
                  (reload/wrap-reload #'app)
                  app)]
    (future (timer-task))
    (run-server handler {:port 8080 :host "127.0.0.1"})))
