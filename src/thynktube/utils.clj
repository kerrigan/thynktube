(ns thynktube.utils)



(defn generate-nick []
  (let [consonants (vec "bcdfghjklmnpqrstvwxyz")
        vowels (vec "aeiou")]
    (loop [letters 6
           nick ""]
      (if (> letters 0)
        (if (= (mod letters 2) 1)
          (recur (- letters 1) (str nick (rand-nth consonants)))
          (recur (- letters 1) (str nick (rand-nth vowels)))
          )
        nick))))


(def yt-video-regex #"^https?://(?:www\.)?youtube.com/watch\?v=([\w0-9_-]+)(&\S*)?$")
(def yt-playlist-regex #"^https?://(?:www\.)?youtube.com/playlist\?list=([\w0-9_-]+)(&\S*)?$")


(defn youtube-url-type [url]
  (cond (re-matches yt-video-regex url) :video
        (re-matches yt-playlist-regex url) :playlist
        :else :unknown))


(defn parse-youtube-url [url]
  (if-let [[_ video-id _] (re-matches yt-video-regex url)]
    [:video video-id]
    (if-let [[_ playlist-id _] (re-matches yt-playlist-regex url)]
      [:playlist playlist-id]
      [:unknown])))


;;https://gdata.youtube.com/feeds/api/playlists/4DAEFAF23BB3CDD0?alt=jsonc&v=2


;;Search
;;http://gdata.youtube.com/feeds/api/videos?prettyprint=true&alt=json&v=2
