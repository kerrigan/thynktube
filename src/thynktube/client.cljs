(ns thynktube.client
  (:require [enfocus.core :as ef]
            [enfocus.events :as events]
            [enfocus.effects :as effects]
            [clojure.string :as str]
            [ajax.core :refer [GET POST json-format url-request-format]]
            [cljs.core.match]
            [goog.string :as gstring]
            [goog.string.format]
            [swfobject :as swf]
            )
  (:use [cljs.reader :only (read read-map read-string)])
  (:require-macros [enfocus.macros :as em]
                   [cljs.core.match.macros :refer [match]]))


(defn by-id [id]
  (.getElementById js/document (name id)))

(defn log [m]
  (.log js/console m))

(def yt-video-regex #"^https?://(?:www\.)?youtube.com/watch\?v=([\w0-9_-]+)(&\S*)?$")
(def yt-playlist-regex #"^https?://(?:www\.)?youtube.com/playlist\?list=([\w0-9_-]+)(&\S*)?$")


(declare yt-regex)
(declare player*)

(defn format-time [duration]
  (if (>= duration (* 60 60))
    (gstring/format "%02d:%02d:%02d" (quot duration (* 60 60))
            (quot (mod duration (* 60 60)) 60)
            (mod duration 60))
    (gstring/format "%02d:%02d" (quot duration 60)
            (mod duration 60))))


(defn string-color-hash [hashed-string]
  (let [string-hash (gstring/hashCode hashed-string)
        red (bit-shift-right (bit-and string-hash 0xFF0000) 16)
        green (bit-shift-right (bit-and string-hash 0x00FF00) 8)
        blue (bit-and string-hash 0x0000FF)
        args (conj (map #(.toString % 16) [red green blue]) "#")
        ]
      (apply gstring/buildString args)))


(defn validate-message [message]
  (let [length (count message)]
    (if (> length 100)
      (do
        (js/alert "Сообщение слишком длинное")
        false)
      (if (< length 2)
        (do (js/alert "Сообщение слишком короткое")
          false)
        true))))


;;;TEMPLATES {{{

(em/defsnippet playlist-item :compiled "public/templates/playlist.html"
               ["table#playlist > tbody > *:last-child"]
               [video]
               "td:first-child" (ef/content (:title video))
               "td:nth-child(2)" (ef/content (format-time (:duration video)))
               "td:last-child"  (ef/content (:added video)))

(em/defsnippet playlist :compiled "public/templates/playlist.html"
               ["table#playlist"]
               [videos]
               "tbody" (ef/content (map #(playlist-item %) videos))
               "tbody > tr:first-child" (ef/add-class "info"))


(em/defsnippet chatmessage :compiled "public/templates/chat.html"
               ["table#chatlog > tbody > *:last-child"]
               [message]
               "td:first-child" (ef/content (:added message))
               "td:last-child" (ef/html-content
                                (gstring/format "<span style='color:%s'>%s</span>: %s"
                                                (str (string-color-hash (:nick message)))
                                                (:nick message)
                                                (-> message :message (gstring/htmlEscape false)))))



(em/defsnippet chatlog :compiled "public/templates/chat.html"
               ["table#chatlog"]
               [messages]
               "tbody" (ef/content (map #(chatmessage %) messages)))



(em/defsnippet chat-user :compiled "public/templates/chat.html"
               ["table#chatuserlist > tbody > *:last-child"]
               [user]
               "td:first-child" (ef/content user))



(em/defsnippet chat-userlist :compiled "public/templates/chat.html"
               ["table#chatuserlist"]
               [users]
               "tbody" (ef/content (map #(chat-user %) users)))


;;}}} END TEMPLATES


;;ACTIONS {{{


(em/defaction change [msg]
              ["#testbtn"] (ef/content msg))


(em/defaction resize-div [width]
              ["#rz-demo"] (effects/chain
                            (effects/resize width :curheight 100)
                            (effects/resize 5 :curheight 500)))



(em/defaction init-forms []
              ["#add-video-url"]
              (events/listen
               :click
               #(do (.preventDefault %)
                  (let [form-data (ef/from "#add-video-form" (ef/read-form))
                        video-url (:video-url form-data)]
                    (if (or (re-matches yt-video-regex video-url)
                            (re-matches yt-playlist-regex video-url))
                      (do
                        (ef/at ["#add-video-url"] (ef/set-attr :disabled "disabled"))
                        (POST "/addvideo" {:params {:video-url video-url}
                                           :format (url-request-format)
                                           :handler (fn [response]
                                                      (ef/at ["#add-video-url"] (ef/remove-attr :disabled)
                                                             ["#video-url"] (ef/set-prop :value "")))
                                           :error-handler (fn [{:keys [status status-text]}]
                                                            (log status))}))
                      (js/alert "Ссылка на видео/плейлист неверна")
                      )
                      )))

              ["#mute"] (events/listen
                         :click #(let [isMuted
                                       (ef/from "#mute"
                                                (ef/get-prop :checked))]
                                   (when-let [player @player*]
                                     (if isMuted
                                       (.mute player)
                                       (.unMute player)))))


              ["#add-chat-message"]
              (events/listen
               :click
               #(do (.preventDefault %)
                  (let [form-data (ef/from "#add-message-form" (ef/read-form))
                        message (:message-text form-data)
                        ]
                    ;(log message)

                    (when (validate-message (-> message gstring/trim))
                      (do
                        (ef/at ["#add-chat-message"] (ef/set-attr "disabled"))
                        (POST "/addmessage" {:params {:message message}
                                                      :format (url-request-format)
                                                      :handler (fn [response]
                                                                 (ef/at ["#add-chat-message"]
                                                                        (ef/remove-attr :disabled)
                                                                        ["#message-text"]
                                                                        (ef/set-prop :value "")))
                                                      :error-handler (fn [{:keys [status status-text]}]
                                                                       (log status))}))


                      )))))

;;}}} END ACTIONS


(def ws-cmd* (atom nil))
(def ws-chat* (atom nil))
(def player* (atom nil))
(def progress-timer* (atom nil))
(def bus-ping-timer* (atom nil))
(def chat-ping-timer* (atom nil))


(def yt-regex #"^https?://(?:www\.)?youtube.com/watch\?v=([\w0-9_-]+)(&\S*)?$")

(defn setup []
  (GET "/playlist" {:handler (fn [response]
                               (do (ef/at ["#playlist-container"]
                                          (ef/content (playlist response)))
                                   (init-forms)))
                    :keywords? true})
  (ef/at ["#mute"] (ef/set-prop :checked false)))


(defn video-progress-loop [duration progress]
  "Запуск прогрессбара видео"
  (when (<= progress duration)
  (let [progressbar-width (* (/ progress duration) 100.0)
        width (str progressbar-width "%")
        pb (by-id "video-progress")]
    (set! (.-width (.-style pb)) width)
    (reset! progress-timer* (js/setTimeout (fn [] (video-progress-loop duration (+ progress 1))) 1000)))))


(defn receive-command [msg]
  "Обработка сообщений проигрывателя"
  (let [msg (read-string msg)]

    (match msg
           [:connected "ok" :nick nick] (ef/at ["#nick"] (ef/content (str "<h2>Добро пожаловать, " nick "</h2>")))
           [:reload-playlist] (GET "/playlist"
                                   {:handler (fn [response]
                                               (ef/at ["#playlist-container"]
                                                      (ef/content (playlist response))
                                                      ))
                                    :keywords? true})
           [:next-video video-id :duration duration] (do
                                                       (when @progress-timer*
                                                         (js/clearTimeout @progress-timer*)
                                                         )
                                                       (.loadVideoById @player* video-id 0 "large")
                                                       (video-progress-loop duration 0)
                                                       )
           [:start-video video-id :duration duration :offset offset] (do
                                                                       (when @progress-timer*
                                                                         (js/clearTimeout @progress-timer*))
                                                                       (.loadVideoById @player* video-id offset "large")
                                                                       (video-progress-loop duration offset))
           [:ping "pong"] (log "pong from cmd bus")
           [:error message] (js/alert message)
           :else (log "Wrong format"))))


(defn scroll-chatlog-to-bottom []
  "Прокрутка лога к низу"
  (ef/at ["#chatlog-container"]
         (ef/set-prop :scrollTop
                      (ef/from "#chatlog-container"
                               (ef/get-prop :scrollHeight)))))


(defn receive-chat-command [msg]
  "Обработка сообщений чата"
  (let [msg (read-string msg)]
    (match msg
           [:users users :messages messages]
           (do (ef/at ["#userlist-container"] (ef/content (chat-userlist users))
                  ["#chatlog-container"] (ef/content (chatlog messages)))
             (scroll-chatlog-to-bottom))
           [:new-message message] (do (ef/at ["#chatlog > tbody"]
                                         (ef/append (chatmessage message)))
                                    (scroll-chatlog-to-bottom))
           [:chat-users users] (ef/at ["#userlist-container"]
                                           (ef/content (chat-userlist users)))
           [:ping "pong"] (log "pong from chat bus")
           :else (log "Wrong format")
           )))

(def current-host (-> js/window .-location .-host))


(defn setup-ws []

    (reset! ws-cmd* (js/WebSocket. (gstring/format "ws://%s/ws" current-host)))

    (reset! ws-chat* (js/WebSocket. (gstring/format "ws://%s/ws/chat" current-host)))

    (doall
     (map #(aset @ws-cmd* (first %) (second %))
          [["onopen" (fn [] (do
                              (log "OPENED COMMAND BUS")
                              (reset! bus-ping-timer*
                                      (js/setInterval
                                       (fn [] (.send @ws-cmd* (str [:ping "ping"]))) 10000))))]
           ["onclose" (fn []
                        (js/setTimeout (fn [] (setup-ws))  8000)
                        (js/clearInterval @bus-ping-timer*)
                        (log "CLOSED COMMAND BUS"))]
           ["onerror" (fn [e] (log (str "ERROR:" e)))]
           ["onmessage"
            (fn [m]
              (let [data (.-data m)]
                ;;Process incoming events
                (receive-command data)
                ;(log data)
                ))]]))

    (doall
     (map #(aset @ws-chat* (first %) (second %))
          [["onopen" (fn [] (do (log "OPEN")
                              (reset! chat-ping-timer*
                                      (js/setInterval
                                       (fn [] (.send @ws-chat* (str [:ping "ping"]))) 10000))))]
           ["onclose" (fn []
                        (log "CLOSE")
                        (js/clearInterval @chat-ping-timer*)
                        (js/setTimeout (fn [] (setup-ws))  8000)
                        )]
           ["onerror" (fn [e] (log (str "ERROR:" e)))]
           ["onmessage"
            (fn [m]
              (let [data (.-data m)]
                ;;Process incoming chat messages
                (receive-chat-command data)
                ;(log data)
                ))]])))

(defn ^:export ytPlayerReady [playerId]
  (let [ytplayer (by-id "player")]
    (setup)
    (reset! player* ytplayer)
    (setup-ws)))


(aset js/window "onYouTubePlayerReady" ytPlayerReady)


(defn init []
  (let [player (by-id "player")
        params (js-obj "allowScriptAccess" "always")
        atts (js-obj "id" "player")]
    (.embedSWF js/swfobject "https://www.youtube.com/apiplayer?version=3&enablejsapi=1"
                        "player" "100%" "400" "8" nil nil params atts)))


(set! (.-onload js/window) (em/wait-for-load (init)))
