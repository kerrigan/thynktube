(ns thynktube.storage
   (:require
   monger.joda-time
   [monger.collection :as mc]
   [monger.query :refer :all]
   [clj-time.core :as t]
   [clj-time.format :as tf]
   )

  (:refer-clojure :exclude [sort find])
  )

(defn load-playlist []
  (map #(select-keys % [:id :title :duration :added :order])
       (with-collection "playlist"
         (fields [:id :title :duration :added :order])
         (sort (sorted-map :order 1)))))


(defn load-chatlog []
  (vec
   (map #(assoc % :added (tf/unparse (tf/formatters :hour-minute-second) (:added %)))
        (map #(select-keys % [:added :message :nick])
             (with-collection "chatlog"
               (fields [:added :message :nick])
               (sort (sorted-map :added 1)))))))



