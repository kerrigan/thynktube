(defproject thynktube "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"
  :url "http://exampl.com/FIXME"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/core.async "0.1.298.0-2a82a1-alpha"]
                 [org.clojure/data.json "0.2.4"]
                 [ring/ring-devel "1.1.8"]
                 [ring/ring-core "1.1.8"]
                 [compojure "1.1.5"]
                 [http-kit "2.1.16"]
                 [cljs-ajax "0.2.3"]
                 [ring/ring-json "0.3.1"]
                 [org.clojure/clojurescript "0.0-2202"]
                 [enfocus "2.0.2"]
                 [com.novemberain/monger "1.8.0"]
                 [org.clojure/core.match "0.2.1"]
                 [clj-time "0.7.0"]
                 ]
  :plugins [[lein-cljsbuild "1.0.3"]
            [lein-ring "0.8.3"]]

  ;:git-dependencies [["https://github.com/UndeRus/enfocus.git"]]

  :cljsbuild {:builds [{:source-paths ["src"]
                        :incremental true
                        :compiler {
                                   :warnings true
                                   ;;:output-dir "out"
                                   :output-to "resources/public/js/main.js"
                                   :externs ["resources/public/libs/youtubeplayer.js"]
                                   :foreign-libs [{:file "resources/public/libs/swfobject.js"
                                                   :provides ["swfobject"]}]
                                   :optimizations :advanced
                                   :pretty-print false
                                   ;;;:source-map true
                                   }
                        }]}
  :omit-source true
  :main thynktube.server
  :aot [thynktube.server]
  )
